import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'WeatherData.dart';
import 'ForecastData.dart';

import 'screenOne.dart';
import 'screenTwo.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState()=>MyAppState();

}

class MyAppState extends State<MyApp> {
  bool isLoading = false;
  WeatherData weatherData;
  ForecastData forecastData;
  Location _location = new Location();
  String error;

  @override
  void initState() {
    super.initState();

    loadWeather();
  }
  @override
  Widget build(BuildContext context) {
    //Lista de pantallas a mostrar
    try {
      List<Widget> screen = [weatherData != null ? ScreenOne(weatherData: weatherData) :Container(), weatherData != null ? ScreenTwo(weather: weatherData,forecast: forecastData) :Container()];
      return new MaterialApp(
        title: 'Flutter Carousel',
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
            body: PageView.builder(
          itemBuilder: (context, position) => screen[position],
          itemCount: screen.length,
        )),
      );
    } on Exception {
     
        new Text("Load Failed");
      
    }
  }
  loadWeather() async {
    setState(() {
      isLoading = true;
    });

    Map<String, double> location;

    try {
      location = await _location.getLocation();

      error = null;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'Permission denied - please ask the user to enable it from the app settings';
      }

      location = null;
    }

    if (location != null) {
      final lat = location['latitude'];
      final lon = location['longitude'];
      print(lat);
      print(lon);
      final forecastResponse = await http.get(
          'https://api.darksky.net/forecast/a3d2e2f0438c5ee6d4bcf19338ae91b2/${lat.toString()},${lon.toString()}?units=si');

      if (forecastResponse.statusCode == 200) {
        return setState(() {
          weatherData = new WeatherData.fromJson(jsonDecode(forecastResponse.body));
          forecastData =new ForecastData.fromJson(jsonDecode(forecastResponse.body));
          isLoading = false;
        });
      }
    }

    setState(() {
      isLoading = false;
    });
  }
}
