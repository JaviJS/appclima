import 'package:flutter/material.dart';
import 'WeatherData.dart';
import 'hexcolor.dart';

class ScreenOne extends StatelessWidget {
  ScreenOne({Key key, @required this.weatherData}) : super(key: key);
  WeatherData weatherData;

  // Pantalla de colores
  final Color color1 = HexColor("ae0100");
  final Color color2 = HexColor("FF0000");
  final Color color3 = HexColor("FF7300");
  final Color color4 = HexColor("FFE600");
  final Color color5 = HexColor("ffff6f");
  final Color color6 = HexColor("CCFFEB");
  final Color color7 = HexColor("A6EDFF");
  final Color color8 = HexColor("4CDBFF");
  final Color color9 = HexColor("0070FF");
  final Color color10 = HexColor("000099");

  Widget _info() {
    return Container(
        padding: const EdgeInsets.all(30.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(
                      '${weatherData.temp}',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 80,
                          shadows: [
                            Shadow(
                                offset: Offset(-2.5, -2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(2.5, -2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(2.5, 2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(-2.5, 2.5),
                                blurRadius: 4.0,
                                color: Colors.black)
                          ]),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 30.0, right: 10.0),
                    child: Text(
                      '°',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 40.0,
                          shadows: [
                            Shadow(
                                offset: Offset(-2.5, -2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(2.5, -2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(2.5, 2.5),
                                blurRadius: 4.0,
                                color: Colors.black),
                            Shadow(
                                offset: Offset(-2.5, 2.5),
                                blurRadius: 4.0,
                                color: Colors.black)
                          ]),
                    ),
                  ),
                  Image.asset('assets/' + getIcono(weatherData.icon),
                      height: 70.0, width: 70.0),
                ],
              ),
              
            ]));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          new Container(
              alignment: Alignment.center,
              color: color1,
              child: _buildChild1()),
          new Container(
            color: color2,
            alignment: FractionalOffset.center,
            child: _buildChild2(),
          ),
          new Container(
              color: color3,
              alignment: FractionalOffset.center,
              child: _buildChild3()),
          new Container(
              color: color4,
              alignment: FractionalOffset.center,
              child: _buildChild4()),
          new Container( 
              color: color5, 
              child: _buildChild5()),
          new Container(
              color: color6,
              alignment: FractionalOffset.center,
              child: _buildChild6()),
          new Container(
              color: color7,
              alignment: FractionalOffset.center,
              child: _buildChild7()),
          new Container(
              color: color8,
              alignment: FractionalOffset.center,
              child: _buildChild8()),
          new Container(
              color: color9,
              alignment: FractionalOffset.center,
              child: _buildChild9()),
          new Container(
              color: color10,
              alignment: FractionalOffset.center,
              child: _buildChild10()),
     
        ],
      ),
    );
  }

  Widget _buildChild1() {
    String temperatura = weatherData.temp
        .toString(); // Se coloca adentro para que sea variable en el tiempo y no se mantenga estatico
   if (int.parse(temperatura) >= 35) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild2() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) >= 30 && int.parse(temperatura) <= 34) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild3() {
    String temperatura = weatherData.temp;
     if (int.parse(temperatura) >= 25 && int.parse(temperatura) <= 29) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild4() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) >= 20 && int.parse(temperatura) <= 24) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild5() {
    String temperatura = weatherData.temp;
     if (int.parse(temperatura) >= 15 && int.parse(temperatura) <= 19) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild6() {
    String temperatura = weatherData.temp;
   if (int.parse(temperatura) >= 10 && int.parse(temperatura) <= 14) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild7() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) >= 9 && int.parse(temperatura) <= 6) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild8() {
    String temperatura = weatherData.temp;
     if (int.parse(temperatura) >= 5 && int.parse(temperatura) <= 1) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild9() {
    String temperatura = weatherData.temp;
   if (int.parse(temperatura) >= 0 && int.parse(temperatura) <= -5) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild10() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) <= -6) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild11() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) >= -55 && int.parse(temperatura) <= -46) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild12() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) >= -65 && int.parse(temperatura) <= -56) {
      return _info();
    }
    return ListTile();
  }

  Widget _buildChild13() {
    String temperatura = weatherData.temp;
    if (int.parse(temperatura) <= -66) {
      return _info();
    }
    return ListTile();
  }

  String getIcono(String nomIcon) {
    if (nomIcon == "clear-day") {
      return "01d.png";
    } else if (nomIcon == "clear-night") {
      return "01n.png";
    } else if (nomIcon == "partly-cloudy-day") {
      return "02d.png";
    } else if (nomIcon == "partly-cloudy-night") {
      return "02n.png";
    } else if (nomIcon == "cloudy") {
      return "03.png";
    } else if (nomIcon == "wind") {
      return "04.png";
    } else if (nomIcon == "fog") {
      return "05.png";
    } else if (nomIcon == "sleet") {
      return "06.png";
    } else if (nomIcon == "rain") {
      return "07.png";
    } else if (nomIcon == "snow") {
      return "08.png";
    } else {
      return "02d.png";
    }
  }
}
