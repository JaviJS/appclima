class WeatherData {
  final int time;
  final String temp;
  final String humidity;
  final String wind;
  final String precipitation;
  final String icon;

  WeatherData({this.time,this.temp, this.humidity, this.wind, this.precipitation, this.icon});

  factory WeatherData.fromJson(Map<String, dynamic> json){
    return WeatherData(
      time: json['currently']['time'],
      temp: json['currently']['temperature'].toString().split('.')[0].toString(),
      humidity: json['currently']['humidity'].toString(),
      wind: json['currently']['windSpeed'].toString(),
      precipitation: json['currently']['precipProbability'].toString(),
      icon: json['currently']['icon'],
    );

  }

}