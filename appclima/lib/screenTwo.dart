import 'package:appclima/hexcolor.dart';
import 'package:flutter/material.dart';
import 'WeatherData.dart';
import 'ForecastData.dart';
import 'package:intl/intl.dart';

class ScreenTwo extends StatelessWidget {
  ScreenTwo({Key key, @required this.weather, @required this.forecast})
      : super(key: key);
  final WeatherData weather;
  final ForecastData forecast;

//child: weatherData != null ? WeatherDay(weather: weatherData) : Container(),
// child: forecastData != null ? ForecastDays(forecast: forecastData) : Container(),
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      child: Column(
        children: <Widget>[
          Container(
            color: getBackground(weather.temp),
            height: MediaQuery.of(context).size.height / 2.4,
            //height: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          '${weather.temp}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none,
                              color: Colors.white,
                              fontSize: 100.0,
                              shadows: [
                                Shadow(
                                    offset: Offset(-2.5, -2.5),
                                    blurRadius: 4.0,
                                    color: Colors.black),
                                Shadow(
                                    offset: Offset(2.5, -2.5),
                                    blurRadius: 4.0,
                                    color: Colors.black),
                                Shadow(
                                    offset: Offset(2.5, 2.5),
                                    blurRadius: 4.0,
                                    color: Colors.black),
                                Shadow(
                                    offset: Offset(-2.5, 2.5),
                                    blurRadius: 4.0,
                                    color: Colors.black)
                              ]),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(bottom: 30.0, right: 10.0),
                          child: Text(
                            '°',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 50.0,
                                shadows: [
                                  Shadow(
                                      offset: Offset(-2.5, -2.5),
                                      blurRadius: 4.0,
                                      color: Colors.black),
                                  Shadow(
                                      offset: Offset(2.5, -2.5),
                                      blurRadius: 4.0,
                                      color: Colors.black),
                                  Shadow(
                                      offset: Offset(2.5, 2.5),
                                      blurRadius: 4.0,
                                      color: Colors.black),
                                  Shadow(
                                      offset: Offset(-2.5, 2.5),
                                      blurRadius: 4.0,
                                      color: Colors.black)
                                ]),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Image.asset('assets/' + getIcono(weather.icon),
                            height: 80.0, width: 80.0),
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Text(
                                '${(weather.humidity)}',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 25.0,
                                    shadows: [
                                      Shadow(
                                          offset: Offset(-1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(-1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black)
                                    ]),
                              ),
                              Text(
                                '%',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 13.0,
                                    shadows: [
                                      Shadow(
                                          offset: Offset(-1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(-1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black)
                                    ]),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 15.0),
                          child: Image.asset('assets/Gota.png',
                              height: 40.0, width: 40.0),
                        )
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Text(
                                '${(weather.precipitation)}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 25.0,
                                    shadows: [
                                      Shadow(
                                          offset: Offset(-1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(-1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black)
                                    ]),
                              ),
                              Text(
                                '%',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13.0,
                                    shadows: [
                                      Shadow(
                                          offset: Offset(-1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, -1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black),
                                      Shadow(
                                          offset: Offset(-1.2, 1.2),
                                          blurRadius: 2.0,
                                          color: Colors.black)
                                    ]),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 15.0),
                          child: Image.asset('assets/Precipitacion.png',
                              height: 45.0, width: 45.0),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                            child: Row(
                          children: <Widget>[
                            Text(
                              '${(weather.wind)}',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.0,
                                  shadows: [
                                    Shadow(
                                        offset: Offset(-1.2, -1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(1.2, -1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(1.2, 1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(-1.2, 1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black)
                                  ]),
                            ),
                            Text(
                              'm/s',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13.0,
                                  shadows: [
                                    Shadow(
                                        offset: Offset(-1.2, -1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(1.2, -1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(1.2, 1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black),
                                    Shadow(
                                        offset: Offset(-1.2, 1.2),
                                        blurRadius: 2.0,
                                        color: Colors.black)
                                  ]),
                            ),
                          ],
                        )),
                        Container(
                          padding: EdgeInsets.only(left: 20.0, right: 15.0),
                          child: Image.asset('assets/Wind.png',
                              height: 50.0, width: 50.0),
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        //width: 100.0,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[1].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(new DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[1].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                )),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[1].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[1].tempMin +
                                    "° | " +
                                    forecast.data[1].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[2].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[2].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[2].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[2].tempMin +
                                    "° | " +
                                    forecast.data[2].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[3].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[3].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[3].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[3].tempMin +
                                    "° | " +
                                    forecast.data[3].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[4].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[4].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[4].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[4].tempMin +
                                    "° | " +
                                    forecast.data[4].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[5].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[5].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[5].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[5].tempMin +
                                    "° | " +
                                    forecast.data[5].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 3.8,
                        width: MediaQuery.of(context).size.width / 3.6,
                        margin: const EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: getBackground(forecast.data[6].tempMax)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                getDia(DateFormat.EEEE().format(
                                    new DateTime.fromMillisecondsSinceEpoch(
                                        forecast.data[6].time * 1000))),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            Image.asset(
                                'assets/' + getIcono(forecast.data[6].icon),
                                height: 60.0,
                                width: 60.0),
                            new Text(
                                forecast.data[6].tempMin +
                                    "° | " +
                                    forecast.data[6].tempMax +
                                    "°",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }

  String getIcono(String nomIcon) {
    if (nomIcon == "clear-day") {
      return "01d.png";
    } else if (nomIcon == "clear-night") {
      return "01n.png";
    } else if (nomIcon == "partly-cloudy-day") {
      return "02d.png";
    } else if (nomIcon == "partly-cloudy-night") {
      return "02n.png";
    } else if (nomIcon == "cloudy") {
      return "03.png";
    } else if (nomIcon == "wind") {
      return "04.png";
    } else if (nomIcon == "fog") {
      return "05.png";
    } else if (nomIcon == "sleet") {
      return "06.png";
    } else if (nomIcon == "rain") {
      return "07.png";
    } else if (nomIcon == "snow") {
      return "08.png";
    } else {
      return "02d.png";
    }
  }

  // Pantalla de colores
  final Color color1 = HexColor("ae0100");
  final Color color2 = HexColor("FF0000");
  final Color color3 = HexColor("FF7300");
  final Color color4 = HexColor("FFE600");
  final Color color5 = HexColor("ffff6f");
  final Color color6 = HexColor("CCFFEB");
  final Color color7 = HexColor("A6EDFF");
  final Color color8 = HexColor("4CDBFF");
  final Color color9 = HexColor("0070FF");
  final Color color10 = HexColor("000099");

  Color getBackground(String temperatura) {
    if (int.parse(temperatura) >= 35) {
      return color1;
    } else if (int.parse(temperatura) >= 30 && int.parse(temperatura) <= 34) {
      return color2;
    } else if (int.parse(temperatura) >= 25 && int.parse(temperatura) <= 29) {
      return color3;
    } else if (int.parse(temperatura) >= 20 && int.parse(temperatura) <= 24) {
      return color4;
    } else if (int.parse(temperatura) >= 15 && int.parse(temperatura) <= 19) {
      return color5;
    } else if (int.parse(temperatura) >= 10 && int.parse(temperatura) <= 14) {
      return color6;
    } else if (int.parse(temperatura) >= 9 && int.parse(temperatura) <= 6) {
      return color7;
    } else if (int.parse(temperatura) >= 5 && int.parse(temperatura) <= 1) {
      return color8;
    } else if (int.parse(temperatura) >= 0 && int.parse(temperatura) <= -5) {
      return color9;
    } else if (int.parse(temperatura) <= -5) {
      return color10;
    }
  }

  String getDia(String dia) {
    if (dia == "Monday") {
      return dia = "Lunes";
    } else if (dia == "Tuesday") {
      return dia = "Martes";
    } else if (dia == "Wednesday") {
      return dia = "Miércoles";
    } else if (dia == "Thursday") {
      return dia = "Jueves";
    } else if (dia == "Friday") {
      return dia = "Viernes";
    } else if (dia == "Saturday") {
      return dia = "Sábado";
    } else {
      return dia = "Domingo";
    }
  }
}
