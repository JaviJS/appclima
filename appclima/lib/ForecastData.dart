class ForecastData {
  final List<Data> data;

  ForecastData({this.data});

  factory ForecastData.fromJson(Map<String, dynamic> json){
    var list= json['daily']['data'] as List;
    List<Data> dataList = list.map((i) => Data.fromJson(i)).toList();
    return  ForecastData(
        data: dataList
    );
  }
}

class Data {
  final int time;
  final String tempMax;
  final String tempMin;
  final String icon;
  Data({this.time, this.tempMax,this.tempMin,this.icon});
  factory Data.fromJson(Map<String, dynamic> json){
    return Data(
        time:json['time'],
        tempMax:json['temperatureHigh'].toString().split('.')[0].toString(),
        tempMin:json['temperatureMin'].toString().split('.')[0].toString(),
        icon:json['icon']
    );
  }
}